import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    padding: 10,
    paddingTop: 30,
  },

  SafeAreaView: {
    flex: 1,
  },

  header: {
    backgroundColor: '#F1F1F1',
    padding: 20,
    paddingTop: 50,
  },

  textHeader: {
    fontSize: 28,
    fontWeight: 'bold',
    color: '#000',
  },

  item: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingHorizontal: 10,

    elevation: 3,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    borderRadius: 12,
    height: 100,
    alignItems: 'center',
  },

  ImageN: {
    width: 50,
    height: 50,
    borderRadius: 50,
  },

  itemText: {
    paddingRight: 20,
    paddingLeft: 10,
  },

  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});
