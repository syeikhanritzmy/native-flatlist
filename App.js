import React, { useState, useEffect } from 'react';
import style from './style/style';
import { View, Text, Image, SafeAreaView, FlatList } from 'react-native';
export default function App() {
  const [data, setData] = useState([]);
  const getUsers = async () => {
    try {
      const res = await fetch('https://randomuser.me/api/?page=1&results=10');
      const json = await res.json();
      setData([...json.results]);
    } catch (error) {}
  };
  useEffect(() => {
    getUsers();
  }, []);

  const Item = ({ item }) => (
    <View style={style.container}>
      <View style={style.item}>
        <View>
          <Image
            style={style.ImageN}
            source={{ uri: `${item.picture.thumbnail}` }}
          />
        </View>
        <View style={style.itemText}>
          <Text
            style={style.title}
          >{`${item.name.title} ${item.name.first} ${item.name.last}`}</Text>

          <Text>
            {`${item.location.street.number} ${item.location.street.name} ${item.location.city} ${item.location.state}`}
          </Text>

          <Text style={{ color: '#f43' }}>{item.email}</Text>
        </View>
      </View>
    </View>
  );
  return (
    <SafeAreaView style={style.SafeAreaView}>
      <View style={style.header}>
        <Text style={style.textHeader}>Customers</Text>
      </View>
      <FlatList
        data={data}
        renderItem={Item}
        keyExtractor={(item) => {
          return (
            item.email.toString() +
            new Date().getTime().toString() +
            Math.floor(
              Math.random() * Math.floor(new Date().getTime())
            ).toString()
          );
        }}
      />
    </SafeAreaView>
  );
}
